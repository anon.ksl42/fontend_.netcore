﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mr_Book_FontEnd_V1.Models
{
    public class AccountModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
