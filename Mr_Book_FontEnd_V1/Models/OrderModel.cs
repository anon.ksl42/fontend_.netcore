﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mr_Book_FontEnd_V1.Models
{
    public class OrderModel
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public int Price { get; set; }
        public string Firstname { get; set; }
        public string ProductName { get; set; }
    }
}
