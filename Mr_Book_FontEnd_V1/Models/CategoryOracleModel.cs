﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mr_Book_FontEnd_V1.Models
{
    public class CategoryOracleModel
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
