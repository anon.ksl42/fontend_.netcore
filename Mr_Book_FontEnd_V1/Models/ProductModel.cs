﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mr_Book_FontEnd_V1.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDetail { get; set; }
        public int ProductPrice { get; set; }
        public string Path_UploadEbook { get; set; }
        public string Path_ImageCover { get; set; }
        public int CateID { get; set; }
        public string CateName { get; set; }
    }
}
