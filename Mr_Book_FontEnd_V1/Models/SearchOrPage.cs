﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mr_Book_FontEnd_V1.Models
{
    public class SearchOrPage
    {
        public int page { get; set; }
        public string search { get; set; }
    }
}
