﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mr_Book_FontEnd_V1.Models;
using Newtonsoft.Json;

namespace Mr_Book_FontEnd_V1.Controllers
{
    public class MysqlController : Controller
    {
        public const string URL = "http://localhost:61944/api/v1/ProductMysql/";
        public IActionResult Index()
        {
            List<AdminModel> model = new List<AdminModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                var data = client.GetAsync("AdminList");
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    model = JsonConvert.DeserializeObject<List<AdminModel>>(res);
                }
            }

            return View(model);
        }
        
        [HttpGet]
        public JsonResult Detail(int id)
        {
            AdminModel model = new AdminModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string content = JsonConvert.SerializeObject(id);
                HttpContent inputContent = new StringContent(content,Encoding.UTF8,"application/json");
                var data = client.PostAsync("DetailAdmin", inputContent);
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                   var res = result.Content.ReadAsStringAsync().Result;
                   model = JsonConvert.DeserializeObject<AdminModel>(res);
                }
            }

            return Json(model);
        }

        [HttpPost]
        public JsonResult RegisterAdmin(AdminModel acc)
        {
            var res = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string content = JsonConvert.SerializeObject(acc);
                HttpContent inputContent = new StringContent(content, Encoding.UTF8, "application/json");
                var data = client.PostAsync("InsertAdmin", inputContent);
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    res = result.Content.ReadAsStringAsync().Result;
                }
            }
            return Json(res.ToString());
        }

        [HttpPost]
        public JsonResult EditAdmin(AdminModel acc)
        {
            var res = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string content = JsonConvert.SerializeObject(acc);
                HttpContent inputContent = new StringContent(content, Encoding.UTF8, "application/json");
                var data = client.PostAsync("EditAdmin", inputContent);
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    res = result.Content.ReadAsStringAsync().Result;
                }
            }
            return Json(res.ToString());
        }

        [HttpPost]
        public JsonResult DeleteAdmin(int id)
        {
            var res = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string content = JsonConvert.SerializeObject(id);
                HttpContent inputContent = new StringContent(content, Encoding.UTF8, "application/json");
                var data = client.PostAsync("DeleteAdmin", inputContent);
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    res = result.Content.ReadAsStringAsync().Result;
                }
            }
            return Json(res.ToString());
        }
    }
}