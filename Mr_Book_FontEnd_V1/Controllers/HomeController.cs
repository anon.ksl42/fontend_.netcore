﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mr_Book_FontEnd_V1.Models;
using Newtonsoft.Json;

namespace Mr_Book_FontEnd_V1.Controllers
{
    [EnableCors("MyCORSPolicy")]
    public class HomeController : Controller
    {
        public const string URL = "http://localhost:61944/api/v1/Product/";
        private IHostingEnvironment hostingEnvironment;

        public HomeController(IHostingEnvironment environment)
        {
            hostingEnvironment = environment;
        }

        public IActionResult Index(int page)
        {
            AllProductModel model = new AllProductModel();

            if(page == 0)
            {
                page = 1;
                HttpContext.Session.SetInt32("page", 1);
            }
            else
            {
                HttpContext.Session.SetInt32("page", page);
            }
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string inputJson = JsonConvert.SerializeObject(page);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var call = client.PostAsync("Book", inputContext);
                call.Wait();

                var result = call.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    model = JsonConvert.DeserializeObject<AllProductModel>(res);
                }
            }
            ViewBag.Sum = model.listBook.Sum(x => x.ProductPrice);
            return View(model);
        }


        [HttpGet]
        public IActionResult Order(SearchOrPage model)
        {
            AllBuyModel model2 = new AllBuyModel();
            if (model.page == 0)
            {
                model.page = 1;
                HttpContext.Session.SetInt32("page1", 1);
            }
            else
            {
                HttpContext.Session.SetInt32("page1", model.page);
            }
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                string inputJson = JsonConvert.SerializeObject(model);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var data = client.PostAsync("Order", inputContext);
                data.Wait();

                var result = data.Result;
                if(result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;
                    model2 = JsonConvert.DeserializeObject<AllBuyModel>(res);
                    
                }
            }
            return View(model2);
        }
        public IActionResult AddProduct()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Detail(int ProductId)
        {
            ProductModel model = new ProductModel();
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                string inputJson = JsonConvert.SerializeObject(ProductId);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var postTask = client.PostAsync("Detail", inputContext);

                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    model = JsonConvert.DeserializeObject<ProductModel>(res);
                }


                return View(model);
            }
        }


        [HttpGet]
        public IActionResult Edit(int ProductId)
        {
            ProductModel model = new ProductModel();
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                string inputJson = JsonConvert.SerializeObject(ProductId);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var postTask = client.PostAsync("Detail", inputContext);

                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    model = JsonConvert.DeserializeObject<ProductModel>(res);
                }

                HttpContext.Session.SetString("detail", model.ProductDetail);
                return View(model);
            }
        }


        [HttpPost]
        public IActionResult Delete(int ProductId)
        {

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                string inputJson = JsonConvert.SerializeObject(ProductId);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var postTask = client.PostAsync("Delete", inputContext);

                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    
                    return Json(true);
                }


                return Json(new { data = false });
            }

        }
    }
}
 
