﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mr_Book_FontEnd_V1.Models;
using Newtonsoft.Json;

namespace Mr_Book_FontEnd_V1.Controllers
{
    public class AccountController : Controller
    {
        public const string URL = "http://localhost:61944/api/v1/Account/";
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Index(AccountModel model)
        {

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                string inputJson = JsonConvert.SerializeObject(model);
                HttpContent inputContext = new StringContent(inputJson, Encoding.UTF8, "application/json");

                var postTask = client.PostAsync("Login", inputContext);
                postTask.Wait();

                var result = postTask.Result;
                if(result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    HttpContext.Session.SetString("Username",model.Username);
                }

            }
            if (HttpContext.Session.GetString("Username") != null)
            {
                return Json(new { data = true});
            }
            else
            {
                return Json(new { data = false });
            }
            
        }
        public IActionResult RegisterAdmin()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RegisterAdmin(AdminModel acc)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                string content = JsonConvert.SerializeObject(acc);
                HttpContent inputContent = new StringContent(content, Encoding.UTF8, "application/json");
                var data = client.PostAsync("Register", inputContent);
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;
                    if (res == "success")
                    {
                        return RedirectToAction("Index","Account");  
                    }
                    else
                    {
                        ViewBag.Error = "Register ไม่สำเร็จ";
                    }
                }
            }

            return View();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Username");

            return RedirectToAction("Index", "Account");
        }
    }
}