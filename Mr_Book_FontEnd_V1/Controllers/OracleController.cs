﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mr_Book_FontEnd_V1.Models;
using Newtonsoft.Json;

namespace Mr_Book_FontEnd_V1.Controllers
{
    public class OracleController : Controller
    {
        public const string URL = "http://localhost:61944/api/v1/ProductOracle/";
        public IActionResult Index()
        {
            List<CategoryOracleModel> model = new List<CategoryOracleModel>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Clear();

                var data = client.GetAsync("CateOracle");
                data.Wait();

                var result = data.Result;
                if (result.IsSuccessStatusCode)
                {
                    var res = result.Content.ReadAsStringAsync().Result;

                    model = JsonConvert.DeserializeObject<List<CategoryOracleModel>>(res);
                }
            }

            return View(model);
        }
    }
}