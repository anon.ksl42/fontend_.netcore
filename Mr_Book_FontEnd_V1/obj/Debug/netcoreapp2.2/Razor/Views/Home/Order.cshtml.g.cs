#pragma checksum "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ab1737e6c9d7a512da781a104b0674c78aa6f048"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Order), @"mvc.1.0.view", @"/Views/Home/Order.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Order.cshtml", typeof(AspNetCore.Views_Home_Order))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\_ViewImports.cshtml"
using Mr_Book_FontEnd_V1;

#line default
#line hidden
#line 2 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\_ViewImports.cshtml"
using Mr_Book_FontEnd_V1.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ab1737e6c9d7a512da781a104b0674c78aa6f048", @"/Views/Home/Order.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bd641c9dbe90401a8d604960c7e2b48dadbd4377", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Order : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Mr_Book_FontEnd_V1.Models.AllBuyModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "get", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(46, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
  
    ViewData["Title"] = "Order";

#line default
#line hidden
            BeginContext(89, 1403, true);
            WriteLiteral(@"<style>
    .center {
        text-align: center;
    }

    .pagination {
        display: inline-block;
    }

    .pagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        border: 1px solid #ddd;
    }

    .pagination a:first-child {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    .pagination a:last-child {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }
</style>
<div class=""container-fluid"">
    <div class=""page-header"">
        <div class=""row align-items-end"">
            <div class=""col-lg-8"">
                <div class=""page-header-title"">
                    <i class=""ik ik-file-text bg-blue""></i>
                    <div class=""d-inline"">
                        <h5>Order</h5>
                        <span>รายการซื้อสินค้า</span>
                    </div>
                </div>
            </div>

            <div cl");
            WriteLiteral(@"ass=""col-lg-4"">
                <nav class=""breadcrumb-container"" aria-label=""breadcrumb"">
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item"">
                            <a href=""#""><i class=""ik ik-home""></i></a>
                        </li>
                        <li class=""breadcrumb-item"">
                            <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1492, "\"", 1526, 1);
#line 53 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
WriteAttributeValue("", 1499, Url.Action("Index","Home"), 1499, 27, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1527, 474, true);
            WriteLiteral(@">Home</a>
                        </li>
                        <li class=""breadcrumb-item active"" aria-current=""page"">Order</li>
                    </ol>
                </nav>
            </div>

        </div>
    </div>
    <div class=""card-body"">
        <div class=""card"">
            <div class=""card-header row"">
                <div class=""col col-sm-2"">
                    <div class=""card-search with-adv-search dropdown"">
                        ");
            EndContext();
            BeginContext(2001, 267, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ab1737e6c9d7a512da781a104b0674c78aa6f0486322", async() => {
                BeginContext(2020, 241, true);
                WriteLiteral("\r\n                            <input type=\"text\" class=\"form-control\" placeholder=\"Search..\" name=\"search\" id=\"search\">\r\n                            <button class=\"btn btn-icon\"><i class=\"ik ik-search\"></i></button>\r\n                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2268, 229, true);
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div class=\"col col-sm-3\">\r\n                </div>\r\n            </div>\r\n            <div class=\"card-body p-0\">\r\n                <div class=\"list-item-wrap\">\r\n");
            EndContext();
#line 78 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                     if (Model.order != null)
                    {
                        

#line default
#line hidden
#line 80 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                         foreach (var i in Model.order)
                        {

#line default
#line hidden
            BeginContext(2651, 212, true);
            WriteLiteral("                            <div class=\"list-item\">\r\n                                <div class=\"item-inner\">\r\n                                    <div class=\"list-title\"><a href=\"javascript:void(0)\">ชื่อลูกค้า: ");
            EndContext();
            BeginContext(2864, 11, false);
#line 84 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                                                                                                Write(i.Firstname);

#line default
#line hidden
            EndContext();
            BeginContext(2875, 228, true);
            WriteLiteral("</a></div>\r\n                                </div>\r\n\r\n                                <div class=\"qickview-wrap\">\r\n                                    <div class=\"desc\">\r\n                                        <p>ชื่อหนังสือ:  ");
            EndContext();
            BeginContext(3104, 13, false);
#line 89 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                                                    Write(i.ProductName);

#line default
#line hidden
            EndContext();
            BeginContext(3117, 58, true);
            WriteLiteral("</p>\r\n                                        <p>วันที่:  ");
            EndContext();
            BeginContext(3176, 54, false);
#line 90 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                                               Write(Convert.ToDateTime(i.OrderDate).ToString("dd/MM/yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(3230, 56, true);
            WriteLiteral("</p>\r\n                                        <p>ราคา:  ");
            EndContext();
            BeginContext(3287, 7, false);
#line 91 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                                             Write(i.Price);

#line default
#line hidden
            EndContext();
            BeginContext(3294, 130, true);
            WriteLiteral(" บาท</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n");
            EndContext();
#line 95 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                        }

#line default
#line hidden
#line 95 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
                         
                    }

#line default
#line hidden
            BeginContext(3474, 70, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n        ");
            EndContext();
            BeginContext(3544, 195, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ab1737e6c9d7a512da781a104b0674c78aa6f04811825", async() => {
                BeginContext(3600, 132, true);
                WriteLiteral("\r\n            <div class=\"center\">\r\n                <div class=\"pagination\">\r\n\r\n                </div>\r\n            </div>\r\n        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "action", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#line 101 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
AddHtmlAttributeValue("", 3571, Url.Action("Order","Home"), 3571, 27, false);

#line default
#line hidden
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3739, 142, true);
            WriteLiteral("\r\n    </div>\r\n</div>\r\n\r\n<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js\"></script>\r\n<script>\r\n    var pageLen = ");
            EndContext();
            BeginContext(3882, 11, false);
#line 113 "C:\Users\Anon\source\repos\MR_BOOK_FontEnd\Mr_Book_FontEnd_V1\Views\Home\Order.cshtml"
             Write(Model.count);

#line default
#line hidden
            EndContext();
            BeginContext(3893, 402, true);
            WriteLiteral(@";
    var limit = 10;
    var i = 0;
    var item = [];
    for (i; i < pageLen / limit; i++){

        item.push(i+1);
        if (pageLen % limit > 0) {
            i + 1;
        }
    }

    $.each(item, function (index, value) {
        $("".pagination"").append('<button type=""submit"" class=""btn "" id=""btn"" name=""page"" value=""'+value+'"">'+value+'</button> ');
    });

</script>

");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Mr_Book_FontEnd_V1.Models.AllBuyModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
