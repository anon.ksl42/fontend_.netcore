﻿$(document).ready(function () {

    function isNullOrEmpty(s) {
        return (s == null || s === "" || s == undefined)
    }


    $("#btn").click(function (e) {
        e.preventDefault();
        
        var user = $("#Username").val();
        var pass = $("#Password").val();
        var obj = { Username: user, Password: pass };
       
        if (!isNullOrEmpty(user) && isNullOrEmpty(pass) ) {
            'use strict';
            resetToastPosition();
            $.toast({
                heading: 'Error',
                text: 'กรุณากรอก Password',
                showHideTransition: 'slide',
                icon: 'error',
                loaderBg: '#f2a654',
                position: 'top-right'
            })
        }
        else if (isNullOrEmpty(user) && !isNullOrEmpty(pass)) {
            'use strict';
            resetToastPosition();
            $.toast({
                heading: 'Error',
                text: 'กรุณากรอก Username',
                showHideTransition: 'slide',
                icon: 'error',
                loaderBg: '#f2a654',
                position: 'top-right'
            })
        }
        else if (isNullOrEmpty(user) && isNullOrEmpty(pass)) {
            'use strict';
            resetToastPosition();
            $.toast({
                heading: 'Error',
                text: 'กรุณากรอก Username และ Password',
                showHideTransition: 'slide',
                icon: 'error',
                loaderBg: '#f2a654',
                position: 'top-right'
            })
        }
        else {
            $.ajax({
                type: 'POST',
                url: "/Account/Index",
                data: { model: obj },
                success: function (res) {

                    if (res.data == true) {
                        window.location.href = "/Home/Index";
                    }
                    else {
                        'use strict';
                        resetToastPosition();
                        $.toast({
                            heading: 'Error',
                            text: 'Username หรือ Password ไม่ถูกต้อง',
                            showHideTransition: 'slide',
                            icon: 'error',
                            loaderBg: '#f2a654',
                            position: 'top-right'
                        })
                    }
                }
            })
        }
    });
});


