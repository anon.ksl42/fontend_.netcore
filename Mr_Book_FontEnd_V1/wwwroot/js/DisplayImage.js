﻿$('#show').hide();

$("#file").change(function () {

    readURL(this);
    $('#show').show();
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#show').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


