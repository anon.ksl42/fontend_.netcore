﻿
function onDelete(id,pageId) {
    swal({
        title: "คุณต้องการลบ?",
        icon: "warning",
        buttons: ["Cancel", "Delete Now"],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: '/Home/Delete',
                data: { ProductId: id },
                success: function (result) {
                    if (result == true) {
                        swal("ถูกลบแล้ว!", {
                            icon: "success",
                        }).then(function () {
                            //window.location.href = "/TestStocks/Index";
                            window.location.href = "/Home?page=" + pageId +"";
                        });
                    }
                    else {
                        swal({
                            title: "ลบไม่สำเร็จแล้ว",
                            icon: "error",
                            button: "OK!",
                        });
                    }
                }
            });
        }
    });
}